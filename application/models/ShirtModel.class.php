<?php

/**
 * Class ShirtModel
 * @brief request to shirts.io web service api for data
 *
 * @author Sergio Sicari
 */

class ShirtModel extends CurlProcessor{

	const API_KEY  = '?api_key=79db865add05db6c5e9825f5490b0914510450c7';
	const BASE_URL = 'https://www.shirts.io/api/v1/';
	const PRODUCTS = 'products/';
	const CATEGORY = 'category/';

    /**
     * get all categories from api request
     * @return mixed
     */
    public function getCategories(){
		$this->setUrl(self::BASE_URL . self::PRODUCTS . self::CATEGORY . self::API_KEY);
		$oCats = json_decode($this->get());
		return $oCats;
	}

    /**
     * get categories by id from api request
     * @param $nId
     * @return mixed
     */
    public function getCategory($nId){
		$this->setUrl(self::BASE_URL . self::PRODUCTS . self::CATEGORY . $nId . "/" . self::API_KEY);
		$oCat = $this->get();
		return $oCat;
	}

    /**
     * get product by id from api request
     * @param $nId
     * @return mixed
     */
    public function getProduct($nId){
		$this->setUrl(self::BASE_URL . self::PRODUCTS . $nId . "/" . self::API_KEY);
		$oPro = $this->get();
		return $oPro;
	}

}
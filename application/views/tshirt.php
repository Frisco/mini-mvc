<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>T-Shirt demo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-image-gallery.min.css" rel="stylesheet">
    <style type="text/css">

    </style>
	<link href="css/main.css" rel="stylesheet" />
	<link href="css/bootstrap-responsive.css" rel="stylesheet" />
	<link href="css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">

    <script src="js/jquery-1.7.2.min.js" type="text/javascript" ></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" ></script>
    <script src="js/tshirt.js" type="text/javascript" ></script>

  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">

        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Social T-Shirt</a>
                    <div class="dropdown pull-right">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="icon-user"></i> _red_button	
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu pull-right">
              <li><a href="#">Profile</a></li>
              <li class="divider"></li>
              <li><a href="#">Sign Out</a></li>
            </ul>
          </div>
                    <div class="nav-collapse">
            <ul class="nav">
              <li><a href="#">Home</a></li>
              <li><a href="#">My T-Shirt</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        
        <div class="span9">

<div class="span2">
<h2><?=$title?></h2>
</div>

<div class="span7" style="width: 180px; margin-top:15px;">
<div class="btn-group" id="categories">
  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
    Select a model:
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
    <!-- dropdown menu links -->
    <?php foreach($aCategories as $oCategory){ ?>
    	<li><a href="#" onclick="catSel(<?=$oCategory->category_id?>);"><?=$oCategory->name?></a></li>
    <?php } ?>

  </ul>
</div>
<div id="loading" style="float:right; background-image: url('img/loading_32.gif'); width: 32px; height: 32px; visibility: hidden;"></div>
</div>

<div style="clear:both;"></div>

<!-- start images -->
<div id="productItem" class="thumbnails" style="display: none; margin-top: 20px;">

</div>

<div style="clear:both;"></div>
<!-- end images -->


<!-- start product details -->
<div id="details" style="width: 800px; display: none; margin-top: 20px;">
  <p>Select a color below:</p>

</div>

<div style="clear:both;"></div>
<!-- end product details -->

<!-- start definitly preview -->
<div id="definitly" style="width: 900px; display: none; margin-top: 20px;">
  <div id="resume" style="float: right; width: 300px; height: 400px;">
    <p><strong>Model:</strong></p>
    <p id="brand"></p>
    <p><strong>Name:</strong></p>
    <p id="name"></p>
    <p><strong>Description:</strong></p>
    <p id="description"></p>
    <p><strong>Size:</strong></p>
    <p id="size"></p>
  </div>
  <div style="width: 550px; height: 700px;">
    <img id="definitly_img" />    
  </div>
  
</div>

<div style="clear:both;"></div>
<!-- end definitly preview -->

        </div><!--/span-->

          <!-- START SIDEBAR -->
          <div class="span3">


              <div class="well" style="min-height: 150px;">
                  <p class="punderline"><strong>Hai effettuato l' accesso come:</strong></p>
                  <div style="float:left; margin-top: 10px;" class="thumbnail">
                      <img src="http://images.ak.instagram.com/profiles/profile_23934465_75sq_1331633706.jpg" width="96" />
                  </div>
                  <div style="float: left; width: 100px; margin: 20px 10px">
                      <p style="margin-top: 7px;"><a href="index.php?url=profile">Red Button</a></p>
                      <p></p>
                      <p><a href="#">Disconnetti</a></p>
                  </div>
              </div>


              <div style="clear:both;"></div>



              <div class="well">
                  <p class="punderline"><strong>Aggiungi foto da altri Network:</strong></p>
		  <p>...</p>
              </div>

          </div>
          <!-- END SIDEBAR -->

      </div><!--/row-->

        <!-- Modal For Save Album -->
    
        <hr>

        <footer>
            <p>&copy; Company 2012</p>
        </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>

    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>

    <script src="js/bootstrap-collapse.js"></script>
    <script src="http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js"></script>
    <script src="js/bootstrap-image-gallery.min.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>


  </body>
</html>


<?php

/**
 * Classe per il routing delle pagine nell' MVC
 * @brief Gestione instradamento pagine
 *
 * @author Sergio Sicari
 */
class Router 
{
    
    private $registry;
    private $path;
    private $args = array();
    public $file;
    public $controller;
    public $action;
    public $params;
    
    function __construct($registry)
    {
        $this->registry = $registry; 
    }
    
    function setPath($path) 
    {
        /*** check if path is a directory ***/
        if (is_dir($path) == false)
        {
            throw new Exception ('Invalid controller path: `' . $path . '`');
        }
        /*** set the path ***/
        $this->path = $path;
    }
    
    public function loader()
    {
        /*** check the route ***/
        $this->getController();
        
        /*** if the file is not there diaf ***/
        if (is_readable($this->file) == false)
        {
            //echo $this->file;
            die ('404 Not Found');
        }
        
        /*** include the controller ***/
        include $this->file;
        
        /*** a new controller class instance ***/
        $class = $this->controller . 'Controller';
        $controller = new $class($this->registry);
        
        /*** check if the action is callable ***/
        if (is_callable(array($controller, $this->action)) == false)
        {
            $action = 'index';
        }
        else
        {
            $action = $this->action;
        }
        /*** run the action ***/
        //$controller->$action();
        if(isSet($this->params))
            call_user_func_array(array($class, $action), $this->params);
        else
            $controller->$action();
    }
    
    /**
     * Ritorna il controller
     */
    private function getController() {
        
        /*** get the route from the url ***/
        $route = (empty($_GET['url'])) ? '' : $_GET['url'];
        
        if (empty($route))
        {
            $route = 'index';
        }
        else
        {
            /*** get the parts of the route ***/
            $parts = explode('/', $route);
            $this->controller = $parts[0];
            if(isset( $parts[1]))
            {
                //var_dump($parts[1]);
                $this->action = $parts[1];
            }
            /*** if there are controller params ***/
            if(count($parts) > 2){
                $aArgs = array_slice($parts, 2);
                $i=2;
                foreach($aArgs as $aArg){
                    array_shift($aArgs);
                    if($i++%2==1)continue;
                    else
                        $this->params[] = current($aArgs);
                }
            }
        }
        
        if (empty($this->controller))
        {
            $this->controller = 'index';
        }
        
        /*** Get action ***/
        if (empty($this->action))
        {
            $this->action = 'index';
        }
	
        /*** set the file path ***/
        $this->file = $this->path .'/'. $this->controller . 'Controller.php';

    }
    
    
}

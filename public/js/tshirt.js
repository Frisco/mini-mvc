/**
 * Created with JetBrains PhpStorm.
 * User: sergioska
 * Date: 08/10/13
 * Time: 16.31
 * To change this template use File | Settings | File Templates.
 */

function catSel(nId){
    $('#definitly').css('display', 'none');
    $('#definitly').css('visibility', 'hidden');
    $('#details').css('visibility', 'hidden');
    $('#loading').css('visibility', 'visible');
    $.ajax({
        type: "GET",
        url: "index.php?url=tshirt/ajaxcatselector",
        data: "id="+nId,
        async: true,
        success: function(msg){
            $('#loading').css('visibility', 'hidden');
            $('#definitly').css('visibility', 'visible');
            var res = jQuery.parseJSON(msg);
            var images = "";
            $.each(res.result, function(i, val){
                images  += '<div style="float: left; padding: 5px;">';
                images  += '<a href="#" class="thumbnail" onclick="selectShopItem('+i+', '+val.product_id+');" id="thumbnail_'+i+'""><img src="'+val.image+'" width="150" /></a>';
                images  += '<p style="font-size: 13px;">'+val.name+'</p>';
                images  += '</div>';
		if(i==4)return false;
            });
            $('#productItem').html(images);
            $('#productItem').css('display', 'block');

        }
    }).responseText;
}

function selectShopItem(item, product_id){
    $('.thumbnail').css('background-color', 'white');
    $('#thumbnail_'+item).css('background-color', '#0088CC');
    $('#loading').css('visibility', 'visible');
    $('#definitly').css('display', 'none');
    $.ajax({
        type: "GET",
        url: "index.php?url=tshirt/ajaxprodselector",
        data: "id="+product_id,
        async: true,
        success: function(msg){
            $('#loading').css('visibility', 'hidden');
            $('#details').css('visibility', 'visible');
            var res = jQuery.parseJSON(msg);
            var sColors = "<p>Select a color:</p>";
            var brand = res.result.brand;
            var description = res.result.description;
            $.each(res.result.colors, function(i, val){
                sColors += '<div style="float: left; margin: 1px;">';
                sColors += '<a href="#" class="thumbnail" onclick="selectColorItem('+i+', \''+val.front_image+'\', \''+brand+'\', \''+description+'\', \''+val.smallest+'\', \''+val.largest+'\', \''+val.name+'\');" id="color_'+i+'""><div style="background-color: #'+val.hex+'; width: 20px; height: 20px;"></div></a>';
                sColors += '</div>';
            });
            $('#details').html(sColors);
            $('#details').css('display', 'block');
        }
    }).responseText;
}

function selectColorItem(item, img, brand, description, size1, size2, name){
    $('#definitly').css('display', 'none');
    $('#definitly_img').attr('src', img);
    $('#definitly').css('display', 'block');
    $('#brand').html(brand);
    $('#name').html(name);
    $('#description').html(description);
    $('#size').html(size1 + " to " + size2);
}

<?php

 /*** error reporting on ***/
 ob_start();
 error_reporting(E_ALL);

 /*** define the site path constant ***/
 $site_path = realpath(dirname(dirname(__FILE__)));

 define ('__SITE_PATH', $site_path);
 define ('APPLICATION_PATH', '/application/');
 define ('DS', '/');
 /*** include the init.php file ***/
 require_once (__SITE_PATH . APPLICATION_PATH . 'init.php');
 
 /*** load the router ***/
 $registry->router = new Router($registry);
 /*** set the path to the controllers directory ***/
 $registry->router->setPath (__SITE_PATH . APPLICATION_PATH . 'controllers');

 /*** load up the template ***/
 $registry->template = new Template($registry);
 
 /*** load the controller ***/
 $registry->router->loader();
 
 /*** include footer.inc ***/
 ob_end_flush();

 
